'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  const Users =sequelize.define('Users',{
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    lastname: DataTypes.STRING,
    role: DataTypes.STRING,
    image: DataTypes.STRING,
    token: DataTypes.STRING,
    tokenExp: DataTypes.STRING
  },{});


  return Users;
};